@extends('adminlte.master')

@section('title')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Cast</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
@endsection

@section('contents')
<section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Create New Cast</h3>
  
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
            <div class="card card-primary">
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/cast" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input type="text" class="form-control" id="nama" name="nama" value=" {{ old('nama', '') }}"placeholder="Enter Nama" required>
                      @error('nama')
                      <div class="alert alert-danger">{{ $message }} </div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="umur">Umur</label>
                      <input type="text" class="form-control" id="umur" name="umur" value="{{ old('umur', '') }}" placeholder="Enter Umur" required>
                      @error('umur')
                      <div class="alert alert-danger">{{ $message }} </div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="bio">Bio</label>
                      <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', '') }}" placeholder="Enter Bio" required>
                      @error('bio')
                      <div class="alert alert-danger">{{ $message }} </div>
                      @enderror
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
