@extends('adminlte.master')

@section('title')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Table Data</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
@endsection

@section('content')
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Table Cast</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
     @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
     @endif
     <a class="btn btn-primary mb-2" href="/cast/create">Create New Cast</a>
      <table class="table table-bordered">
          <thead>                  
            <tr>
              <th style="width: 10px">No</th>
              <th>Nama</th>
              <th>Bio</th>
              <th style="width: 40px">Umur</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
        @forelse($cast as $key => $casts)
            <tr>
                <td> {{ $key + 1 }} </td>
                <td> {{ $casts -> nama}} </td>
                <td> {{ $casts -> bio}} </td>
                <td> {{ $casts -> umur}} </td>
                <td style="display: flex">
                    <a href="/cast/{{$casts->id}}" class="btn btn-info btn-sm">Show</a>
                    <a href="/cast/{{$casts->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                    <form action="/cast/{{$casts->id}}" method="POST">
                      @csrf
                      @method('DELETE')
                      <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="5" align="center">No Cast</td>
            </tr>
        @endforelse
          </tbody>
        </table>
    </div>
  </div>

  </section>
  @endsection